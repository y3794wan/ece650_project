There are three algorithms used to solve minimum vertex cover problem. 
CNF-SAT-VC is the optimal algorithm to solve VC problem by using the miniSAT.
The other two algorithms only calculate the approximate results.

**Commands:**
cd VC_project && mkdir build && cd build && cmake ../ && make && ./a4ece650

**Input example:**

V 5

E {<1,4>,<0,1>,<1,2>,<2,0>,<4,0>,<3,1>,<3,0>}

**Output:**

CNF-SAT-VC: 0,1

APPROX-VC-1: 0,1

APPROX-VC-2: 0,1,2,4

cnf_sat_res runtime: 303.457

approx_vc1 runtime: 41.225

approx_vc2 runtime: 121.02

Approximation Ratio for APPROX-VC-1: 1.00

Approximation Ratio for APPROX-VC-2: 2.00



The miniSAT used in this project comes from https://github.com/agurfinkel/minisat.git 

Go to the link and clone minisat folder in the top level directory of the project using.
