#include <memory>
#include "minisat/core/SolverTypes.h"
#include "minisat/core/Solver.h"
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <regex>
#include <stdlib.h>
#include <algorithm>
#include <pthread.h>
#include <time.h>
#include <malloc.h>
#include <iomanip>

using namespace std;

typedef pair<int,int> pii;

//Used to store vertex cover solutions
string cnf_sat_res="CNF-SAT-VC: timeout\n";
string approx_vc1="";
string approx_vc2="";

//Used to calculate the approximation ratio
double cnf_size=0;
double approx_1_size=0;
double approx_2_size=0;

//Used to calculate the runtime
double cnf_sat_runtime=0.0;
double approx_vc1_runtime=0.0;
double approx_vc2_runtime=0.0;

//Generate a struct of data about generate a graph
struct Graph_data{
  //the number of vertex
  int num;
  string edges;
  //the assembly of each edge
  //pay attention to the blank
  vector<vector<string> > edge;
  vector<vector<int> > sum_quene;
};

//Used to store arguments for threads
struct Thread_Args{
  //V: the number of vertex
  unsigned int V;
  //Edges: the list of all edges
  vector<vector<int> > Edges;
  //High_degree: the list of different degrees of vertexs(decreasing)
  vector<int> high_degree;
};

//======================Functions========================//

//Convert the string to int in the edge vector
vector<vector<int> > convertInt(const vector<vector<string> > &vectorEdge){
  vector<vector<int> > edge_int;
  vector<int> eachedge;
  for(unsigned int i=0;i<vectorEdge.size();i++){
    for(unsigned int j=0;j<vectorEdge[i].size();j++){
      int n=atoi(vectorEdge[i][j].c_str());
      eachedge.push_back(n);
    }
    edge_int.push_back(eachedge);
    eachedge.clear();
  }  
  return edge_int;
}

//Sorting
bool cmp(pii a,pii b){
  return a.second>b.second;
}
//Calculate the degree of vertexs
vector<int> Degree(const vector<vector<int> > &vertexdegree,const int &num){
  //search for those vertexs which have at least one edge
  //key:the number of vertex
  //value:the degree of each vertex
  map<int,int> mapvertex;
  for(int i=0;i<num;i++){
    int count=0;
    for(unsigned int j=0;j<vertexdegree.size();j++){
      vector<int> vec=vertexdegree[j];
      vector<int>::iterator it=find(vec.begin(),vec.end(),i);
      if(it!=vec.end()){
	count=count+1;
      }
      vec.clear();
    }
    if(count!=0){
      mapvertex.insert(map<int,int>::value_type(i,count));
    }
  }
  //sort the degree(value)
  vector<pii> value_vec;
  vector<pii>::iterator vit;
  map<int,int>::iterator mit;
  for(mit=mapvertex.begin();mit!=mapvertex.end();mit++){
    value_vec.push_back(pii(mit->first,mit->second));
  }
  sort(value_vec.begin(),value_vec.end(),cmp);
  //add the vertex to vector(in a decreasing sequence)
  vector<int> point_degree;
  for(vit=value_vec.begin();vit!=value_vec.end();vit++){
    point_degree.push_back(vit->first);
  }
  return point_degree;
}

//=========================Three algorithms==============================//

//APPROX-VC-1
void* approx_1(void* args){
  struct timespec ts;
  clockid_t cid;
  //Start timer
  pthread_getcpuclockid(pthread_self(),&cid);
  //initialize the final vertex cover vector of approx1
  vector<int> approx1_result;
  //convert the value of strcut to some seperate variables
  struct Thread_Args *args_list=(struct Thread_Args *)args;
  unsigned int num=args_list->V;
  vector<vector<int> > Edge=args_list->Edges;
  vector<int> K=args_list->high_degree;
  
  unsigned int x=0;
  while(x<num){
    int cvalue=K[0];
    //delete all edges related to the highest degree point
    for(vector<vector<int> >::iterator it1=Edge.begin();it1!=Edge.end();){
      vector<int> current_edge=(*it1);
      if(current_edge[0]==cvalue || current_edge[1]==cvalue){
	it1=Edge.erase(it1);
      }
      else
	it1++;
    }
    //put valid vertex into vertex cover
    approx1_result.push_back(cvalue);
    //judge whether finished
    if(Edge.size()==0){
      break;
    }
    else{
      K.clear();
      K=Degree(Edge,num);
      x++;
    }
  }
  //sorting the vertex cover and print
  sort(approx1_result.begin(),approx1_result.end());
  approx_1_size=approx1_result.size();
  //output vertex cover
  string result="APPROX-VC-1: ";
  for(unsigned int i=0;i<approx1_result.size()-1;i++){
    result=result+to_string(approx1_result[i])+",";
  }
  result=result+to_string(approx1_result[approx1_result.size()-1])+"\n";
  approx_vc1=result;
  approx1_result.clear();
  
  //Stop timer
  clock_gettime(cid,&ts);
  approx_vc1_runtime=(((double)ts.tv_nsec)*0.001);
  //Relase the memory
  free(args);
  pthread_exit(NULL);
}

//approx2
void* approx_2(void* args){
  struct timespec ts;
  clockid_t cid;
  //Start timer
  pthread_getcpuclockid(pthread_self(),&cid);
  
  vector<int> approx2_result;
  //convert the value of strcut to some seperate variables
  struct Thread_Args *args_list=(struct Thread_Args *)args;
  unsigned int num=args_list->V;
  vector<vector<int> > edge=args_list->Edges;
  vector<int> K=args_list->high_degree;
  
  unsigned int x=0;
  while(x<num){
    int u=edge[0][0];
    int v=edge[0][1];
    approx2_result.push_back(u);
    approx2_result.push_back(v);
    //delete the edges related with u
    for(vector<vector<int> >::iterator it1=edge.begin();it1!=edge.end();){
      vector<int> current_edge1=(*it1);
      if(current_edge1[0]==u || current_edge1[1]==u){
	it1=edge.erase(it1);
      }
      else
	it1++;
    }
    //delete the edges related with v
    for(vector<vector<int> >::iterator it2=edge.begin();it2!=edge.end();){
      vector<int> current_edge2=(*it2);
      if(current_edge2[0]==v || current_edge2[1]==v){
	it2=edge.erase(it2);
      }
      else
	it2++;
    }
    //judge
    if(edge.size()==0){
      break;
    }
    else{
      x++;
    }
  }
  //sorting the vertex cover and print
  sort(approx2_result.begin(),approx2_result.end());
  approx_2_size=approx2_result.size();
  //output vertex cover
  string result="APPROX-VC-2: ";
  for(unsigned int i=0;i<approx2_result.size()-1;i++){
    result=result+to_string(approx2_result[i])+",";
  }
  result=result+to_string(approx2_result[approx2_result.size()-1])+"\n";
  approx_vc2=result;
  approx2_result.clear();
  
  //Stop timer
  clock_gettime(cid,&ts);
  approx_vc2_runtime=(((double)ts.tv_nsec)*0.001);

  //Release the memory
  free(args);
  pthread_exit(NULL);
}

void* SAT(void* args){
  struct timespec ts;
  clockid_t cid;
  //Start timer
  pthread_getcpuclockid(pthread_self(),&cid);
  
  //convert the value of strcut to some seperate variables
  struct Thread_Args *args_list=(struct Thread_Args *)args;
  unsigned int num=args_list->V;
  vector<vector<int> > Edge=args_list->Edges;
  vector<int> Cover=args_list->high_degree;
  
  vector<int> Vertex;
  vector<int> vc;
  for(unsigned int i=0;i<num;i++){
    Vertex.push_back(i);
  }
  // -- allocate on the heap so that we can reset later if needed
  unique_ptr<Minisat::Solver> solver(new Minisat::Solver());
  //k is in the K(current vertex cover)
  vector<int> K;
  vector<vector<Minisat::Lit> > database(num,vector<Minisat::Lit>(num));
  for(unsigned int a=0;a<Cover.size();a++){
      K.push_back(Cover[a]);
      //every element in matrix is literal     
      for(unsigned int i=0;i<num;i++){
	for(unsigned int j=0;j<num;j++){
	  Minisat::Lit literal;
	  literal =Minisat::mkLit(solver->newVar());
	  database[i][j]=literal;
	}
      }  
      //clause1: At least one vertex is the ith vertex in the vertex cover
      for(unsigned int i=0;i<K.size();i++){
	Minisat::vec<Minisat::Lit> clause1;
	for(unsigned int j=0;j<Vertex.size();j++){
	  int I=K[i];
	  clause1.push(database[j][I]);
	}
	solver->addClause(clause1);
	clause1.clear();
      }
      //clause2: No one vertex can appear twice in a vertex cover
      if(K.size()>1){
	for(unsigned int m=0;m<Vertex.size();m++){
	  for(unsigned int p=0;p<K.size();p++){
	    for(unsigned int q=0;q<K.size();q++){
	      int P=K[p];
	      int Q=K[q];
	      if(P<Q){
		solver->addClause(~database[m][P],~database[m][Q]);
	      }
	    }
	  }
	}
      }
      //clause3: No more than one vertex appears
      //in the mth position of vertex cover
      for(unsigned int m=0;m<K.size();m++){
	int M=K[m];
	for(unsigned int p=0;p<Vertex.size();p++){
	  for(unsigned int q=0;q<Vertex.size();q++){
	    if(p<q){
	      solver->addClause(~database[p][M],~database[q][M]);
	    }
	  }
	}
      }
      //clause4:Every edge is incident to
      //at least one vertex in the vertex cover
      Minisat::vec<Minisat::Lit> clause2;
      for(unsigned int i=0;i<Edge.size();i++){
	int a=Edge[i][0];
	int b=Edge[i][1];
	for(unsigned int k=0;k<K.size();k++){
	  int x=K[k];
	  clause2.push(database[a][x]);
	  clause2.push(database[b][x]);
	}
	solver->addClause(clause2);
	clause2.clear();
      }
      //judge whether the NPC is SAT
      bool res = solver->solve();
      if(res==true){
     	for(unsigned int x=0;x<Vertex.size();x++){
	  for(unsigned int y=0;y<K.size();y++){
	    int z=K[y];
	    bool lit_val=Minisat::toInt(solver->modelValue(database[x][z]));
	    if(lit_val==0){
	      vc.push_back(x);
	    }
	  }
	}
	//SAT 
	solver.reset (new Minisat::Solver());
	break;
      }    
      else{
	//UNSAT
	// the next line de-allocates existing solver and allocates a new
	// one in its place.	  
	solver.reset (new Minisat::Solver());
      }   	
    }
  //sorting the vertex cover and print
  sort(vc.begin(),vc.end());
  cnf_size=vc.size();
  //output vertex cover
  string result="CNF-SAT-VC: ";
  for(unsigned int i=0;i<vc.size()-1;i++){
    result=result+to_string(vc[i])+",";
  }
  result=result+to_string(vc[vc.size()-1])+"\n";
  cnf_sat_res=result;
  vc.clear();
  
  //Stop timer
  clock_gettime(cid,&ts);
  cnf_sat_runtime=(((double)ts.tv_nsec)*0.001);

  //Release the memory
  free(args);
  pthread_exit(NULL);
}

//=================Threading==================//

void thread_vc(unsigned int num,vector<vector<int> > Edgeslist,vector<int> K){
  
  //Initialization
  struct Thread_Args *args_cnf_sat=(struct Thread_Args*)malloc(sizeof(*args_cnf_sat));
  struct Thread_Args *args_vc1=(struct Thread_Args*)malloc(sizeof(*args_vc1));
  struct Thread_Args *args_vc2=(struct Thread_Args*)malloc(sizeof(*args_vc2));
  //Set values for each variables in struct args
  
  if(args_cnf_sat!=NULL){
    args_cnf_sat->V=num;
    args_cnf_sat->Edges=Edgeslist;
    args_cnf_sat->high_degree=K;
  }
  if(args_vc1!=NULL){
    args_vc1->V=num;
    args_vc1->Edges=Edgeslist;
    args_vc1->high_degree=K;
  }
  if(args_vc2!=NULL){
    args_vc2->V=num;
    args_vc2->Edges=Edgeslist;
    args_vc2->high_degree=K;
  }
  
  //Create threads
  pthread_t thread1;
  pthread_t thread2;
  pthread_t thread3;

  //Run threads(multithreading)
  pthread_create(&thread1,NULL,SAT,args_cnf_sat);
  pthread_create(&thread2,NULL,approx_1,args_vc1);
  pthread_create(&thread3,NULL,approx_2,args_vc2);
  
  struct timespec ts;
  clock_gettime(CLOCK_REALTIME,&ts);
  ts.tv_sec +=120;
  
  //Wait for the threads exist
  pthread_timedjoin_np(thread1, NULL, &ts);
  pthread_join(thread2,NULL);
  pthread_join(thread3,NULL);

  //Output
  cout<<cnf_sat_res<<flush;
  cout<<approx_vc1<<flush;
  cout<<approx_vc2<<flush;
  
  cout<<"cnf_sat_res runtime: "<<cnf_sat_runtime<<endl; 
  cout<<"approx_vc1 runtime: "<<approx_vc1_runtime<<endl;  
  cout<<"approx_vc2 runtime: "<<approx_vc2_runtime<<endl;

  cout<<"Approximation Ratio for APPROX-VC-1: "<<fixed<<setprecision(2)<<approx_1_size/cnf_size<<endl;
  cout<<"Approximation Ratio for APPROX-VC-2: "<<fixed<<setprecision(2)<<approx_2_size/cnf_size<<endl;
}

//global variable
Graph_data graph;
vector<string> edges;
vector<string> edge_number;

int main(void) {
  while(!cin.eof()){
    string line;
    getline(cin,line); 
    string cmd;
    istringstream input(line);
    input>>cmd;
    if(cmd=="V"){
      input>>graph.num;
    }
    else if(cmd=="E"){
      input>>graph.edges;
      //split the edge input
      regex regexp("<[0-9]+,[0-9]+>");
      smatch m;
      string::const_iterator iterStart=graph.edges.begin();
      string::const_iterator iterEnd=graph.edges.end();
      while(regex_search(iterStart, iterEnd, m, regexp)){
	edges.push_back(m[0]);
	iterStart=m[0].second;
      }
      //extract the certain vertex of edges and store
      regex pattern("[0-9]+");
      smatch result;
      for(unsigned int i=0;i<edges.size();i++){
	string::const_iterator restart= edges[i].begin();
	string::const_iterator reend= edges[i].end();
	while(regex_search(restart, reend, result, pattern)){
	  edge_number.push_back(result[0]);
	  restart=result[0].second;
	}
	graph.edge.push_back(edge_number);
	edge_number.clear();
      }
      //convert string to int of edge and vertex
      vector<vector<int> > edgeVector=convertInt(graph.edge);     
      int max=0;
      for(unsigned int i=0;i<edgeVector.size();i++){
	for(unsigned int j=0;j<edgeVector[i].size();j++){
	  if(max<=edgeVector[i][j]){
	    max=edgeVector[i][j];
	  }
	}     
      }
      //graph.num is the total number of vertexs
      //max is the maximum number of the point in edge
      //comparing
      if((graph.num-1)<max){
	cerr<<"Error: The graph is not existed.";
      }
      else if(graph.num==1){
	cerr<<"Error: The vertex cover is not exists.";
      }
      else{
	//judge the degree of every vertex
	vector<int> vertex_degree=Degree(edgeVector,graph.num);	
	//run the threads
	//edgeVector is the list of edges
	thread_vc(graph.num,edgeVector,vertex_degree);	
	//clear all vectors and prepare for the next loop
	edges.clear();
	edge_number.clear();
	graph.edge.clear();
      }
    }
  }
  return 0;
}
